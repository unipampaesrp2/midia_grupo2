
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

/**
 * Implementa os métodos da classe BancoMidia e extende os atributos da classe
 * Musica.
 *
 * @author Juliano
 */
public class MusicaDAO extends Musica implements BancoMidia {

    protected Vector<Musica> vMusica;
    private String nome;

    public MusicaDAO() {
        this.vMusica = new Vector();

    }

    @Override
    public String consulta(String nome) {
        int posicao = buscaMidia(nome);
        if (posicao != -1) {
            return "Diretório : " + vMusica.get(posicao).getNomeDoArquivo()
                    + "\nTítulo : " + vMusica.get(posicao).getTitulo()
                    + "\nDescricao : " + vMusica.get(posicao).getDescricao()
                    + "\nAutores : " + vMusica.get(posicao).getAutor()
                    + "\nGênero : " + vMusica.get(posicao).getGenero()
                    + "\nIdioma : " + vMusica.get(posicao).getIdioma()
                    + "\nintérpretes : " + vMusica.get(posicao).getInterprete()
                    + "\nDuração : " + vMusica.get(posicao).getDuracao()
                    + "\nAno : " + vMusica.get(posicao).getAno();
        }
        return "Midia não encontrada";
    }

    public void exclui(int op) {
        vMusica.remove(op);

    }

    @Override
    public void edita(int posObjeto, Midia novoDado) {
        Musica musica = (Musica) novoDado;
        vMusica.get(posObjeto).setNomeDoArquivo(musica.getNomeDoArquivo());
        vMusica.get(posObjeto).setTitulo(musica.getTitulo());
        vMusica.get(posObjeto).setDescricao(musica.getDescricao());
        vMusica.get(posObjeto).setAutor(musica.getAutor());
        vMusica.get(posObjeto).setGenero(musica.getGenero());
        vMusica.get(posObjeto).setIdioma(musica.getIdioma());
        vMusica.get(posObjeto).setInterprete(musica.getInterprete());
        vMusica.get(posObjeto).setDuracao(musica.getDuracao());
        vMusica.get(posObjeto).setAno(musica.getAno());
    }

    @Override
    public void ordena() {
        //elege um e procura o número menor
        Musica aux;
        for (int i = 0; i <= vMusica.size(); i++) {
            for (int a = i + 1; a <= vMusica.size() - 1; a++) {
                if ((vMusica.get(a).getTitulo().compareTo(vMusica.get(i).getTitulo())) <= -1) {
                    aux = vMusica.get(a);
                    vMusica.set(a, vMusica.get(i));
                    vMusica.set(i, aux);
                }
            }
        }
    }

    public void gravaArquivo(String nome) throws IOException {
        ObjectOutputStream output;
        output = new ObjectOutputStream(new FileOutputStream(new File(nome)));
        output.writeObject(vMusica);
        output.close();
    }

    public void lerArquivo(String nome) throws IOException, ClassNotFoundException {
        ObjectInputStream input;
        input = new ObjectInputStream(new FileInputStream(new File(nome)));
        vMusica = (Vector<Musica>) input.readObject();
        input.close();
    }

    public String exibe() throws IOException {
        String lista = "";
        for (Musica vMusica1 : vMusica) {
            lista = lista.concat("Nome do Arquivo = " + vMusica1.getNomeDoArquivo() + "\nTítulo = " + vMusica1.getTitulo() + "\nDescrição = " + vMusica1.getDescricao() + "\nAutor = " + vMusica1.getAutor() + "\nGênero = " + vMusica1.getGenero() + "\nIdioma = " + vMusica1.getIdioma() + "\nIntérprete = " + vMusica1.getInterprete() + "\nAno = " + vMusica1.getAno() + "\n\n");
        }
        return lista;
    }

    /**
     * Busca uma determinada midia, por uma String informada por parâmetro.
     *
     * @param nome titulo da musica para busca.
     * @return posicao do título no Array ou -1 caso não encontre o nome
     * informado.
     */
    public int buscaMidia(String nome) {
        for (int i = 0; i < vMusica.size(); i++) {
            if (vMusica.get(i).getTitulo().equals(nome)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void cadastraMidia(Midia m) {
        vMusica.add((Musica) m);
    }

    /**
     * Valida a duração da música, para que os minutos nao ultrapassem de 59 e
     * os segudos também.
     *
     * @param minutos
     * @param segundos
     * @return
     */
    public boolean validaDuracao(int minutos, int segundos) {
        return (minutos >= 0 && minutos <= 59) && (segundos >= 0 && segundos <= 59);
    }

    /**
     * Valida o ano da música, para que o dado informado fique entre 1600 e
     * 2014.
     *
     * @param ano
     * @return
     */
    public boolean validaAno(int ano) {
        return ano >= 1600 && ano <= 2014;
    }

    //BUSCAS
    public String buscaPorGenero(String genero) {
        String str = "";
        for (Musica vMusica1 : vMusica) {
            if (vMusica1.getGenero().equalsIgnoreCase(genero)) {
                str = str.concat("Nome do Arquivo: " + vMusica1.getNomeDoArquivo() + "\nAutores: " + vMusica1.getAutor() + "\nInterpretes: " + vMusica1.getInterprete() + "\nDuração: " + vMusica1.getDuracao() + "\nAno: " + vMusica1.getAno() + "\nIdioma: " + vMusica1.getIdioma() + "\nTítulo: " + vMusica1.getTitulo() + "\nDescrição: " + vMusica1.getDescricao() + "\nGênero: " + vMusica1.getGenero() + "\n\n");
            }
        }
        return str;
    }

    public String buscaPorAno(String ano) {
        String str = "";
        for (Musica vMusica1 : vMusica) {
            if (vMusica1.getAno().equals(ano)) {
                str = str.concat("Nome do Arquivo: " + vMusica1.getNomeDoArquivo() + "\nAutores: " + vMusica1.getAutor() + "\nInterpretes: " + vMusica1.getInterprete() + "\nDuração: " + vMusica1.getDuracao() + "\nAno: " + vMusica1.getAno() + "\nIdioma: " + vMusica1.getIdioma() + "\nTítulo: " + vMusica1.getTitulo() + "\nDescrição: " + vMusica1.getDescricao() + "\nGênero: " + vMusica1.getGenero() + "\n\n");
            }
        }
        return str;
    }

    public String buscaPorAutor(String autor) {
        String str = "";
        for (Musica vMusica1 : vMusica) {
            if (vMusica1.getAutor().equalsIgnoreCase(autor)) {
                str = str.concat("Nome do Arquivo: " + vMusica1.getNomeDoArquivo() + "\nAutores: " + vMusica1.getAutor() + "\nInterpretes: " + vMusica1.getInterprete() + "\nDuração: " + vMusica1.getDuracao() + "\nAno: " + vMusica1.getAno() + "\nIdioma: " + vMusica1.getIdioma() + "\nTítulo: " + vMusica1.getTitulo() + "\nDescrição: " + vMusica1.getDescricao() + "\nGênero: " + vMusica1.getGenero() + "\n\n");
            }
        }
        return str;
    }
}
