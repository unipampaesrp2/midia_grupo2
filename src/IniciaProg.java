import javax.swing.JOptionPane;

public class IniciaProg {

	public static void main(String[] args) throws Exception {
		MusicaDAO dao = new MusicaDAO();
		PodcastDAO dao1 = new PodcastDAO();
		IPodcast a = new IPodcast(dao1);
		IMusica i = new IMusica(dao);
		EbookDAO dao2 = new EbookDAO();
        IEbook b = new IEbook(dao2);
		
		boolean repete = true;
		while (repete) {
			int op = Integer.parseInt(JOptionPane
					.showInputDialog("1-Musicas\n2-Podcasts\n3- Ebook"));
			switch (op) {
			case 1:
				i.executar();
				break;
			case 2:
				a.executar();
				break;
			case 3:
				b.executar();
				break;
			case 4:
				break;
			case 0:
				repete = false;
				break;
			}
		}

	}
}
