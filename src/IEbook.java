
/*
 * s� teste no bitbucket
 */
import java.awt.Dimension;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
Come�o da Interface do Ebook com o executar,e o nemu inicial.
*/
public class IEbook {

    private EbookDAO dao;

    public IEbook(EbookDAO dao) {
        this.dao = dao;
    }
    
     public void executar() throws IOException, ClassNotFoundException {
         preCadastro();

        boolean repetir = true;
        try {
            dao.ler();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IEbook.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (repetir) {

            int op = Integer.parseInt(JOptionPane.showInputDialog("Digite sua op��o:\n1 - Cadastrar\n2 - Ler\n3 - Excluir\n4 - Editar\n5 - Consultar\n6 - Ordenar dados\n7 Busca por Genero\n8 - Busca por Ano\n9 - Busca por Autor\n10 - Retornar ao menu anterior"));

            switch (op) {
                case 1:
                    cadastraDadosNoArrayList();
                   
                    break;
                    
                    case 2:
                    exibeDadosDoArquivo();
                 
                    break;
                        
                        case 3:
                    removeObjeto();
                    break;
                            
                case 4:
                    editaDadosDoArquivo();
                
                    break;
                    
                case 5:
                    consultaDados();
                    break;

                case 6:
                    ordenaDadosBubbleSort();
                    break;
                    
                case 7:
                    String busca;
                    busca=JOptionPane.showInputDialog("Digite o Genero do Ebook a ser buscado:");
                    if(!dao.buscaPorGenero(busca).equals("")){
                    JTextArea textArea = new JTextArea(dao.buscaPorGenero(busca));
                    JScrollPane scrollPane = new JScrollPane(textArea);
                    textArea.setLineWrap(true);
                    textArea.setWrapStyleWord(true);
                    scrollPane.setPreferredSize (new Dimension (500,500));
                    JOptionPane.showMessageDialog(null, scrollPane, "Ebooks", JOptionPane.PLAIN_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null, "G�nero informado n�o possui cadastro");
                    } 
                    
                
                    break;
                    
                case 8:
                    
                    busca=JOptionPane.showInputDialog("Digite o Ano do Ebook a ser buscado:");
                    if(!dao.buscaPorAno(busca).equals("")){
                    JTextArea textArea = new JTextArea(dao.buscaPorAno(busca));
                    JScrollPane scrollPane = new JScrollPane(textArea);
                    textArea.setLineWrap(true);
                    textArea.setWrapStyleWord(true);
                    scrollPane.setPreferredSize (new Dimension (500,500));
                    JOptionPane.showMessageDialog(null, scrollPane, "Ebooks", JOptionPane.PLAIN_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null, "Ano informado n�o possui cadastro");
                    } 
                    
                
                    break;
                    
                case 9:
                    
                    busca=JOptionPane.showInputDialog("Digite o Autor do Ebook a ser buscado:");
                    if(!dao.buscaPorAutor(busca).equals("")){
                    JTextArea textArea = new JTextArea(dao.buscaPorAutor(busca));
                    JScrollPane scrollPane = new JScrollPane(textArea);
                    textArea.setLineWrap(true);
                    textArea.setWrapStyleWord(true);
                    scrollPane.setPreferredSize (new Dimension (500,500));
                    JOptionPane.showMessageDialog(null, scrollPane, "Ebooks", JOptionPane.PLAIN_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null, "Autor informado n�o possui cadastro");
                    } 
                break;

                case 10:
                    repetir = false;
                    break;
                    
                default:
                    JOptionPane.showMessageDialog(null, "Opera��o Inv�lida", "ERRO", JOptionPane.ERROR_MESSAGE);
                    break;
            }
        }

    }
     /*
     M�todo de ordena��o Bubble Sort.
     */
     private void ordenaDadosBubbleSort() throws IOException {
                dao.ordena();
                JOptionPane.showMessageDialog(null, "Objetos ordenados!");
        }
    /*
     M�todo que cadastra no Array List.
     */
     
     private void cadastraDadosNoArrayList() throws IOException {
        int op;
        String titulo;
        String ano = "";
        String nomeDoArquivo = JOptionPane.showInputDialog("Digite o nome do arquivo");

            titulo = JOptionPane.showInputDialog("Digite novo titulo");

        String genero = JOptionPane.showInputDialog("Digite genero");
        String editora = JOptionPane.showInputDialog("Digite editora");
        String local = JOptionPane.showInputDialog("Digite local");
        String descricao = JOptionPane.showInputDialog("Digite descricao");
        String autor = JOptionPane.showInputDialog("Digite autor");
        int numpag = Integer.parseInt(JOptionPane.showInputDialog("Digite n�mero de p�ginas"));
        while (true) {
           ano = JOptionPane.showInputDialog("Digite ano");
            int iAno = Integer.parseInt(ano);
            if (dao.validaAno(iAno)) {
                break;
            } else {
                JOptionPane.showMessageDialog(null, "Digite um ano v�lido!", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
        Midia e = new Ebook(genero, local, editora, ano, numpag, nomeDoArquivo, titulo, descricao, autor);
        dao.cadastraMidia(e);

    }
     /*
     M�todo que edita os dados do arquivo.
     */
     public void editaDadosDoArquivo() throws IOException {
        String str;
        int op;
        do {
            str = JOptionPane.showInputDialog("Digite o titulo do ebbok para busca de altera��o");
            op = dao.buscaMidia(str);
            if (op != -1) {
                dao.setTitulo(JOptionPane.showInputDialog("Digite o novo t�tulo"));
                dao.setGenero(JOptionPane.showInputDialog("Digite genero"));
                dao.setAutor(JOptionPane.showInputDialog("Digite autor"));
                dao.setEditora(JOptionPane.showInputDialog("Digite editora"));
                dao.setDescricao(JOptionPane.showInputDialog("Digite descricao"));
                dao.setLocal(JOptionPane.showInputDialog("Digite local"));
                dao.setNumpaginas(Integer.parseInt(JOptionPane.showInputDialog("Digite N�mero de p�ginas")));
                while (true) {
                    String ano = JOptionPane.showInputDialog("Digite ano");
                    int iAno = Integer.parseInt(ano);
                    if (dao.validaAno(iAno)) {
                        dao.setAno(ano);
                        break;
                    } else {
                        JOptionPane.showMessageDialog(null, "Digite um ano v�lido!", "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            } else {
                JOptionPane.showMessageDialog(null, "T�tulo da ebbok n�o existe, digite um t�tulo v�lido!!");
            }
        } while (true);

        dao.edita(op, dao);
    }
     /*
     M�todo que exibe os dados no arquivo.
     */
     
     public void exibeDadosDoArquivo() throws IOException, ClassNotFoundException {
        JTextArea textArea = new JTextArea(dao.exibe());
        JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        scrollPane.setPreferredSize(new Dimension(500, 500));
        JOptionPane.showMessageDialog(null, scrollPane, "Ebook", JOptionPane.PLAIN_MESSAGE);

    }
     
     /*
     M�todo remove o objeto.
     */
         public void removeObjeto() throws IOException {
        String str;
        int op;
        str = JOptionPane.showInputDialog("Digite titulo");
        op = dao.buscaMidia(str);
        if (op != -1) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            JOptionPane.showConfirmDialog(null, "Voc� tem certeza que deseja excluir este objeto?", "WARNING", dialogButton);
            if (dialogButton == JOptionPane.YES_OPTION) {
                dao.exclui(op);
                JOptionPane.showMessageDialog(null, "Objeto removido!");
            } else {
                JOptionPane.showMessageDialog(null, "Objeto n�o removido!");
            }

        } else {
            JOptionPane.showMessageDialog(null, "T�tulo do Ebook n�o existe!");
        }

    }
/*
         M�todo consulta os dados.
         */
    private void consultaDados() {
        String str = JOptionPane.showInputDialog("Digite o T�tulo do Ebook a ser consultado");
        JOptionPane.showMessageDialog(null, dao.consulta(str));
        
    }

    private void ordenaDadosBublleSort() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
/*
    Pr� cadastro.
    */

public void preCadastro() throws IOException{

       Midia m1 = new Ebook ("Romance", "Brasil", "America", "2014", 100, "Arquivo", "Amor e guerra", "Melhor Romance", "Paulo,Ricardo\n");
       Midia m2 = new Ebook ("Terror", "Brasil", "Letras", "2013", 100, "Arquivo", "O Massacre", "Melhor Terror", "Jo�o, Paulo\n");
       Midia m3 = new Ebook ("Romance", "EUA", "California", "2012", 100, "Arquivo", "Tudo por voc�", "Melhor atriz", "Carla, Macedo\n");
       Midia m4 = new Ebook ("Guerra", "Alemanha", "America", "2011", 100, "Arquivo", "O Resgate do Soldado", "Melhor ebook de guerra", "Matheus, Resende\n");
       Midia m5 = new Ebook ("Romance", "Brasil", "Ampola", "2014", 100, "Arquivo", "Tudo por amor", "Melhor ator", "Andressa, Aguilar\n");
       Midia m6 = new Ebook ("Suspense", "Jap�o", "Ampla", "2014", 100,"Arquivo", "O olho", "Melhor Suspense", "Carlos, Alberto\n");
       Midia m7 = new Ebook ("Suspense", "EUA", "Letras", "2009", 100,"Arquivo",  "O Espirito", "Pior suspense do ano", "Fl�via, Ribeiro\n");
       Midia m8 = new Ebook ("Romance", "Espanha", "California", "2005", 100, "Arquivo", "A for�a da Paix�o", "Melhor Romance", "Guilherme, Duarte\n");
       Midia m9 = new Ebook ("Terror", "Jap�o", "America", "2014", 100, "Arquivo", "Fim da linha", "Melhor Terror", "Maria, Marta\n");
       Midia m10 = new Ebook ("Terror", "Brasil", "America", "1999", 100, "Arquivo", "Dr�cula", "Melhor Terror", "Erica, Motta\n");

dao.cadastraMidia(m1);
dao.cadastraMidia(m2);
dao.cadastraMidia(m3);
dao.cadastraMidia(m4);
dao.cadastraMidia(m5);
dao.cadastraMidia(m6);
dao.cadastraMidia(m7);
dao.cadastraMidia(m8);
dao.cadastraMidia(m9);
dao.cadastraMidia(m10);


}
}