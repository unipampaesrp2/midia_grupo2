

import java.io.Serializable;

public class Ebook extends Midia implements Serializable {
    
/*
    Atributos da classe Ebook
    */
    private String genero;
    private String local;
    private String editora;
    private String ano;
    private int numpaginas;

    public Ebook(){
    }
    public Ebook(String genero, String local, String editora, String ano, int numpaginas, String nomeDoArquivo, String titulo, String descricao, String autor) {
        super(nomeDoArquivo, titulo, descricao, autor);
        this.genero = genero;
        this.local = local;
        this.editora = editora;
        this.ano = ano;
        this.numpaginas = numpaginas;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public void setNumpaginas(int numpaginas) {
        this.numpaginas = numpaginas;
    }

    public String getGenero() {
        return genero;
    }

    public String getLocal() {
        return local;
    }

    public String getEditora() {
        return editora;
    }

    public String getAno() {
        return ano;
    }

    public int getNumpaginas() {
        return numpaginas;
        
        
        
    }
}