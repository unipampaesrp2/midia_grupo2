
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Implemeta a interface de interação com o usuário.
 *
 * @author Juliano
 */
public class IMusica {

    private MusicaDAO dao;

    public IMusica(MusicaDAO dao) {
        this.dao = dao;
    }

    /**
     * Chama a execução da IMusica.
     *
     * @throws IOException
     */
    public void executar() throws IOException, ClassNotFoundException {

        try {
            dao.lerArquivo("Musica.bin");
        } catch (Exception e) {
        }

        boolean repetir = true;

        while (repetir) {
            int op;

            op = Integer.parseInt(JOptionPane.showInputDialog("Digite sua opção:\n1 - Cadastrar\n2 - Exibir\n3 - Excluir\n4 - Editar\n5 - Consultar\n6 - Ordenar dados\n7 - Busca por gênero \n8 - Busca por autor \n9 - Busca por ano \n10 - Retornar ao menu anterior"));
            switch (op) {
                case 1:
                    cadastraDadosNoArrayList();
                    break;
                case 2:
                    exibeDadosDoArquivo();
                    break;
                case 3:
                    removeObjeto();
                    break;
                case 4:
                    editaDadosDoArquivo();
                    break;
                case 5:
                    consultaDados();
                    break;
                case 6:
                    ordenaDadosSelectionSort();
                    break;
                case 7:
                    String busca;
                    busca = JOptionPane.showInputDialog("Digite o gênero para efetuar a busca");
                    if (!dao.buscaPorGenero(busca).equals("")) {
                        JTextArea textArea = new JTextArea(dao.buscaPorGenero(busca));
                        JScrollPane scrollPane = new JScrollPane(textArea);
                        textArea.setLineWrap(true);
                        textArea.setWrapStyleWord(true);
                        scrollPane.setPreferredSize(new Dimension(500, 500));
                        JOptionPane.showMessageDialog(null, scrollPane, "Musicas", JOptionPane.PLAIN_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Gênero informado não possui cadastro");
                    }
                    break;
                case 8:
                    busca = JOptionPane.showInputDialog("Digite o autor para efetuar a busca");
                    if (!dao.buscaPorAutor(busca).equals("")) {
                        JTextArea textArea = new JTextArea(dao.buscaPorAutor(busca));
                        JScrollPane scrollPane = new JScrollPane(textArea);
                        textArea.setLineWrap(true);
                        textArea.setWrapStyleWord(true);
                        scrollPane.setPreferredSize(new Dimension(500, 500));
                        JOptionPane.showMessageDialog(null, scrollPane, "Musicas", JOptionPane.PLAIN_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Autor informado não possui cadastro");
                    }
                    break;
                case 9:
                    busca = JOptionPane.showInputDialog("Digite o ano para efetuar a busca");
                    if (!dao.buscaPorAno(busca).equals("")) {
                        JTextArea textArea = new JTextArea(dao.buscaPorAno(busca));
                        JScrollPane scrollPane = new JScrollPane(textArea);
                        textArea.setLineWrap(true);
                        textArea.setWrapStyleWord(true);
                        scrollPane.setPreferredSize(new Dimension(500, 500));
                        JOptionPane.showMessageDialog(null, scrollPane, "Musicas", JOptionPane.PLAIN_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Ano informado não possui cadastro");
                    }
                    break;
                case 10:
                    repetir = false;
                    try {
                        dao.gravaArquivo("Musica.bin");
                    } catch (FileNotFoundException exc) {
                        System.out.println("Erro: Banco.bin não encontrado");
                    } catch (Exception e) {
                        System.out.println("Erro: " + e.getMessage());
                    }
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Operação Inválida", "ERRO", JOptionPane.ERROR_MESSAGE);
                    break;
            }

        }
    }

    /**
     * Chama o método responsável pela ordenação, caso o array de músicas não
     * esteja vazio.
     */
    private void ordenaDadosSelectionSort() {
        if (dao.vMusica.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Não há dados inseridos para serem ordenados");
        } else {
            dao.ordena();
            JOptionPane.showMessageDialog(null, "Dados ordenados pelo título!", "Ordenação", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Pergunta para o usuário os dados para serem inseridos no arquivo e chama
     * o método para cadastrar um novo tipo de mídia no array.
     *
     * @throws IOException
     */
    private void cadastraDadosNoArrayList() throws IOException {
        int op;
        String titulo;
        String ano;
        String duracao;
        String nomeDoArquivo = JOptionPane.showInputDialog("Digite nome do arquivo");
        titulo = JOptionPane.showInputDialog("Digite novo titulo");
        String genero = JOptionPane.showInputDialog("Digite genero");
        String Idioma = JOptionPane.showInputDialog("Digite idioma");
        String interprete = JOptionPane.showInputDialog("Digite os interpretes");
        while (true) {
            String minutos = JOptionPane.showInputDialog("Digite os minutos de duração da música: ");
            String segundos = JOptionPane.showInputDialog("Digite os segundos de duração da música: ");
            int min = Integer.parseInt(minutos);
            int seg = Integer.parseInt(segundos);
            if (dao.validaDuracao(min, seg)) {
                duracao = minutos + ":" + segundos;
                break;
            } else {
                JOptionPane.showMessageDialog(null, "Digite um tempo de duração válido!", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
        String descricao = JOptionPane.showInputDialog("Digite descricao");
        String autor = JOptionPane.showInputDialog("Digite os autores");
        while (true) {
            ano = JOptionPane.showInputDialog("Digite ano");
            int iAno = Integer.parseInt(ano);
            if (dao.validaAno(iAno)) {
                break;
            } else {
                JOptionPane.
                        showMessageDialog(null, "Digite um ano válido!", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
        Midia m = new Musica(genero, Idioma, interprete, nomeDoArquivo, titulo, descricao, autor, duracao, ano);
        dao.cadastraMidia(m);

    }

    /**
     * Pergunta para o usuário os novos métodos para serem atribuidos a um
     * objeto e chama o método de editar os dados.
     *
     * @throws IOException
     */
    private void editaDadosDoArquivo() throws IOException {
        if (dao.vMusica.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Não há dados inseridos para serem editados");
        } else {
            String str;
            int op;
            do {
                str = JOptionPane.showInputDialog("Digite o titulo da música para busca de alteração");
                op = dao.buscaMidia(str);
                if (op != -1) {
                    dao.setTitulo(JOptionPane.showInputDialog("Digite o novo título"));
                    dao.setGenero(JOptionPane.showInputDialog("Digite genero"));
                    dao.setIdioma(JOptionPane.showInputDialog("Digite idioma"));
                    dao.setInterprete(JOptionPane.showInputDialog("Digite interprete"));
                    while (true) {
                        String minutos = JOptionPane.showInputDialog("Digite os minutos de duração da música: ");
                        String segundos = JOptionPane.showInputDialog("Digite os segundos de duração da música: ");
                        int min = Integer.parseInt(minutos);
                        int seg = Integer.parseInt(segundos);
                        if (dao.validaDuracao(min, seg)) {
                            String duracao = minutos + ":" + segundos;
                            dao.setDuracao(duracao);
                            break;
                        } else {
                            JOptionPane.showMessageDialog(null, "Digite um tempo de duração válido!", "ERRO", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    dao.setDescricao(JOptionPane.showInputDialog("Digite descricao"));
                    dao.setAutor(JOptionPane.showInputDialog("Digite autor"));
                    while (true) {
                        String ano = JOptionPane.showInputDialog("Digite ano");
                        int iAno = Integer.parseInt(ano);
                        if (dao.validaAno(iAno)) {
                            dao.setAno(ano);
                            break;
                        } else {
                            JOptionPane.showMessageDialog(null, "Digite um ano válido!", "ERRO", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    break;
                } else {
                    JOptionPane.showMessageDialog(null, "Nome da musica não existe, digite um nome válido!!");
                }
            } while (true);

            dao.edita(op, dao);
        }
    }

    /**
     * Instância uma área de texto para exibir os dados do arquivo e chama o
     * método responsável pela exibição do arquivo.
     *
     * @throws IOException
     */
    private void exibeDadosDoArquivo() throws IOException, ClassNotFoundException {
        JTextArea textArea = new JTextArea(dao.exibe());
        JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        scrollPane.setPreferredSize(new Dimension(500, 500));
        JOptionPane.showMessageDialog(null, scrollPane, "Musicas", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * chama o método responsável por excluir um objeto do arquivo, de acordo
     * com o título informado.
     *
     * @throws IOException
     */
    private void removeObjeto() throws IOException {
        if (dao.vMusica.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Não há dados inseridos para serem removidos");
        } else {
            String str;
            int op;
            str = JOptionPane.showInputDialog("Digite titulo");
            op = dao.buscaMidia(str);
            if (op != -1) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                JOptionPane.showConfirmDialog(null, "Você tem certeza que deseja excluir este objeto?", "WARNING", dialogButton);
                if (dialogButton == JOptionPane.YES_OPTION) {
                    dao.exclui(op);
                    JOptionPane.showMessageDialog(null, "Objeto removido!");
                } else {
                    JOptionPane.showMessageDialog(null, "Objeto não removido!");
                }

            } else {
                JOptionPane.showMessageDialog(null, "Título da musica não existe!");
            }

        }
    }

    /**
     * pergunta para o usuário o título da música e chama o método de consulta
     * para exibir os dados de acordo com o título informado.
     */
    private void consultaDados() {
        if (dao.vMusica.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Não há dados inseridos para serem consultados");
        } else {
            String str = JOptionPane.showInputDialog("Digite nome da musica para consultar dados:");
            JOptionPane.showMessageDialog(null, dao.consulta(str));
        }
    }

}
