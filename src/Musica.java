
import java.io.Serializable;

/**
 * Estende os atributos e métodos da classe Midia e define os atributos da classe Musica.
 * @author Juliano
 */
class Musica extends Midia implements Serializable{

    private String genero;
    private String idioma;
    private String interprete;
    private String duracao;
    private String ano;

    public Musica() {
    }

    public Musica(String genero, String idioma, String interprete, String nomeDoArquivo, String titulo, String descricao, String autor, String duracao, String ano) {
        super(nomeDoArquivo, titulo, descricao, autor);
        this.duracao = duracao;
        this.genero = genero;
        this.idioma = idioma;
        this.interprete = interprete;
        this.ano = ano;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public void setInterprete(String interprete) {
        this.interprete = interprete;
    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    public void setAno(String ano) {
        this.ano = ano;

    }

    public String getGenero() {
        return genero;
    }

    public String getIdioma() {
        return idioma;
    }

    public String getInterprete() {
        return interprete;
    }

    public String getDuracao() {
        return duracao;
    }

    public String getAno() {
        return ano;
    }

}