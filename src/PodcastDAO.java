import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.Collator;
import java.util.*;

/**
 * 
 * @author Abner, Juliano e Camila
 *
 */
public class PodcastDAO implements BancoMidia {

	Podcast podcast;
	LinkedList<Podcast> podcasts;

	public PodcastDAO() {
		podcasts = new LinkedList<Podcast>();

	}

	/**
	 * cria um objetos com as informações necessarias e popula o array
	 * especifico de podcasts.
	 * 
	 * @param idioma
	 * @param duracao
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param autor
	 */
	public void cadastraPodcast(String idioma, String duracao,
			String nomeDoArquivo, String titulo, String descricao, String autor) {
		podcast = new Podcast(idioma, duracao, nomeDoArquivo, titulo,
				descricao, autor);
		ordena();
		podcasts.add(podcast);

	}

	/**
	 * exibe as informações contidas na lista.
	 */
	public String exibe() {
		String mostrar = "";
		for (Podcast a : podcasts) {
			mostrar = mostrar.concat("Nome do arquivo: " + a.getNomeDoArquivo()
					+ "\n" + "Titulo: " + a.getTitulo() + "\n" + "Descrição: "
					+ a.getDescricao() + "\n" + "Autor: " + a.getAutor() + "\n"
					+ "Idioma: " + a.getIdioma() + "\n" + "Duração: "
					+ a.getDuracao() + "\n\n");
		}

		return mostrar;

	}

	/**
	 * escreve no arquivo binario o que foi colocado na lista.
	 * 
	 * @throws Exception
	 */
	public void gravar(String arq) throws Exception {

		ObjectOutputStream out;
		out = new ObjectOutputStream(new FileOutputStream(new File(arq)));

		out.writeObject(podcasts);

		out.close();

	}

	/**
	 * le o arquivo binario e popula a lista com as informações. recebe o nome
	 * do arquivo como parametro.
	 * 
	 * @param arq
	 * @throws Exception
	 */
	public void ler(String arq) throws Exception {
		ObjectInputStream in;
		in = new ObjectInputStream(new FileInputStream(new File(arq)));
		podcasts = (LinkedList<Podcast>) in.readObject();
		in.close();

	}

	/**
	 * busca um titulo na lista e retorna sua posição caso não exista retorna
	 * -1.
	 * 
	 * @param nome
	 * @return
	 */
	public int buscaTitulo(String nome) {
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getTitulo().equalsIgnoreCase(nome)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * busca o/os autores/autor e concatena os autores na lista.
	 * 
	 * @param autores
	 * @return
	 */
	public String consultaAutores(String autores) {
		String str = "";
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getAutor().equalsIgnoreCase(autores)) {
				str = str.concat("\n\n" + " Diretorio: "
						+ podcasts.get(i).getNomeDoArquivo() + "\n"
						+ "Titulo: " + podcasts.get(i).getTitulo() + "\n"
						+ "Descrição: " + podcasts.get(i).getDescricao() + "\n"
						+ "Autor: " + podcasts.get(i).getAutor() + "\n"
						+ "Idioma: " + podcasts.get(i).getIdioma() + "\n"
						+ "Duração: " + podcasts.get(i).getDuracao());

			}

		}
		return str;
	}

	/**
	 * busca uma duração e retorna os podcasts com esse duração.
	 * 
	 * @param duracao
	 * @return
	 */
	public String consultaDuracao(String duracao) {
		String str = "";
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getDuracao().equalsIgnoreCase(duracao)) {
				str = str.concat("\n\n" + " Diretorio: "
						+ podcasts.get(i).getNomeDoArquivo() + "\n"
						+ "Titulo: " + podcasts.get(i).getTitulo() + "\n"
						+ "Descrição: " + podcasts.get(i).getDescricao() + "\n"
						+ "Autor: " + podcasts.get(i).getAutor() + "\n"
						+ "Idioma: " + podcasts.get(i).getIdioma() + "\n"
						+ "Duração: " + podcasts.get(i).getDuracao());

			}

		}
		return str;
	}

	/**
	 * busca um idioma na lista, recebido por parametro, e retorna os podcasts
	 * com esse idioma.
	 * 
	 * @param idioma
	 * @return
	 */
	public String consultaIdioma(String idioma) {
		String str = "";
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getIdioma().equalsIgnoreCase(idioma)) {
				str = str.concat("\n\n" + " Diretorio: "
						+ podcasts.get(i).getNomeDoArquivo() + "\n"
						+ "Titulo: " + podcasts.get(i).getTitulo() + "\n"
						+ "Descrição: " + podcasts.get(i).getDescricao() + "\n"
						+ "Autor: " + podcasts.get(i).getAutor() + "\n"
						+ "Idioma: " + podcasts.get(i).getIdioma() + "\n"
						+ "Duração: " + podcasts.get(i).getDuracao());

			}

		}
		return str;
	}

	/**
	 * retorna um podcast com o titulo dado por parametro. caso não exista
	 * retorna uma mensagem.
	 */
	public String consulta(String nome) {
		int posicao = buscaTitulo(nome);

		if (posicao != -1) {
			return "Diretorio: " + podcasts.get(posicao).getNomeDoArquivo()
					+ "\n" + "Titulo: " + podcasts.get(posicao).getTitulo()
					+ "\n" + "Descrição: "
					+ podcasts.get(posicao).getDescricao() + "\n" + "Autor: "
					+ podcasts.get(posicao).getAutor() + "\n" + "Idioma: "
					+ podcasts.get(posicao).getIdioma() + "\n" + "Duração: "
					+ podcasts.get(posicao).getDuracao();

		}

		return "Midia não encontrada";
	}

	/**
	 * ordena a lista.
	 */
	public void ordena() {
		Collator collator = Collator.getInstance();
		collator.setStrength(Collator.PRIMARY);

		for (int j = 1; j < podcasts.size(); j++) {
			int i = j - 1;
			Podcast aux = podcasts.get(j);
			while (i >= 0
					&& collator.compare(aux.getTitulo(), podcasts.get(i)
							.getTitulo()) < 0) {
				podcasts.set(i + 1, podcasts.get(i));
				i--;
			}
			podcasts.set(i + 1, aux);
		}
	}

	/**
	 * metodo de exclusão tendo o titulo como parametro.
	 * 
	 * @param titulo
	 */
	public void exclui(String titulo) {
		podcasts.remove(buscaTitulo(titulo));
	}

	/**
	 * cadastra midia com com objeto podcast
	 */
	public void cadastraMidia(Midia m) {
		podcasts.add((Podcast) m);
	}

	/**
	 * implementação da interface BancoMidia.
	 */
	public void exclui(int op) {
		podcasts.remove(op);

	}

	/**
	 * recebe como parametro o titulo. busca a posição com o titulo.
	 * 
	 * @param titulo
	 * @param novoDado
	 */
	public void edita(String titulo, Midia novoDado) {
		Podcast pod = (Podcast) novoDado;
		int i = buscaTitulo(titulo);
		podcasts.get(i).setTitulo(pod.getTitulo());
		podcasts.get(i).setDescricao(pod.getDescricao());
		podcasts.get(i).setAutor(pod.getAutor());
		podcasts.get(i).setIdioma(pod.getIdioma());
		podcasts.get(i).setDuracao(pod.getDuracao());

	}

	/**
	 * implementação da interface.
	 */
	public void edita(int posObjeto, Midia novoDado) {
		Podcast pod = (Podcast) novoDado;
		podcasts.get(posObjeto).setTitulo(pod.getTitulo());
		podcasts.get(posObjeto).setDescricao(pod.getDescricao());
		podcasts.get(posObjeto).setAutor(pod.getAutor());
		podcasts.get(posObjeto).setIdioma(pod.getIdioma());
		podcasts.get(posObjeto).setDuracao(pod.getDuracao());

	}

}