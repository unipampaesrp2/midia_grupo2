
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
Implementa os m�todos da classe BancoMidia e extende os atributos da classe Ebook.
*/
public class EbookDAO extends Ebook implements BancoMidia {

    private ArrayList<Ebook> alEbook;
   
    private File arq = new File("Ebook.txt");

    public EbookDAO() {
        this.alEbook = new ArrayList();
    }

    @Override
    public String consulta(String nome) {
        int posicao = buscaMidia(nome);
        if (posicao != -1) {
            return "Diret�rio : " + alEbook.get(posicao).getNomeDoArquivo()
                    + "\nT�tulo : " + alEbook.get(posicao).getTitulo()
                    + "\nDescricao : " + alEbook.get(posicao).getDescricao()
                    + "\nAutor : " + alEbook.get(posicao).getAutor()
                    + "\nG�nero : " + alEbook.get(posicao).getGenero()
                    + "\nLocal : " + alEbook.get(posicao).getLocal()
                    + "\nEditora : " + alEbook.get(posicao).getEditora()
                    + "\nNumpag : " + alEbook.get(posicao).getNumpaginas()
                    + "\nAno : " + alEbook.get(posicao).getAno();
        }
        return "Midia n�o encontrada";
    }
/*
    
    */
    @Override
    public void exclui(int op) {
        alEbook.remove(op);

    }
/*
    
    */
    @Override
    public void edita(int posObjeto, Midia novoDado) {
        Ebook eb = (Ebook) novoDado;
        alEbook.get(posObjeto).setTitulo(eb.getTitulo());
        alEbook.get(posObjeto).setDescricao(eb.getDescricao());
        alEbook.get(posObjeto).setAutor(eb.getAutor());
        alEbook.get(posObjeto).setGenero(eb.getGenero());
        alEbook.get(posObjeto).setLocal(eb.getLocal());
        alEbook.get(posObjeto).setEditora(eb.getEditora());
        alEbook.get(posObjeto).setNumpaginas(eb.getNumpaginas());
        alEbook.get(posObjeto).setAno(eb.getAno());
    }
/*
    
    */
    @Override
    public String exibe() {
        String mostrar = "";
        for (Ebook i : alEbook){
            mostrar = mostrar.concat(i.getNomeDoArquivo()+ "\n" + i.getTitulo()+ "\n" + i.getGenero()+"\n" 
                    + i.getEditora()+"\n" + i.getLocal()+"\n" + i.getDescricao()+"\n" + i.getAutor()+"\n" + i.getNumpaginas()+"\n" + i.getAno() + "\n");
        }
        return mostrar;
        
    }
    
    
/*
     Busca uma determinada midia, por uma String informada por par�metro.
     *
     * @param nome titulo do Ebook para busca.
     * @return posicao do t�tulo no Array ou -1 caso n�o encontre o nome
     * informado.
    
    */
    public int buscaMidia(String nome) {
        for (int i = 0; i < alEbook.size(); i++) {
            if (alEbook.get(i).getTitulo().equals(nome)) {
                return i;
            }
        }
        return -1;
    }
    
    /*
    Faz a busca por Genero, Ano e Autor.
    */ 
    public String buscaPorGenero(String genero) {
		String str = "";
        for (Ebook alEbook1 : alEbook) {
            if (alEbook1.getGenero().equalsIgnoreCase(genero)) {
                str = str.concat ("\nNome do Arquivo:" + alEbook1.getNomeDoArquivo() + "\n\t Autor:" + alEbook1.getAutor() + "\n\t Editora: " + alEbook1.getEditora() + "\n\t Local: " + alEbook1.getLocal() + "\n\t ano: " + alEbook1.getAno() + "\n\t Descri��o: " + alEbook1.getDescricao() + "\n\t T�tulo: " + alEbook1.getTitulo() + "\n\t N�mero de p�ginas: " + alEbook1.getNumpaginas() + "\n\t G�nero: " + alEbook1.getGenero());
               
            }
        }
		return str;
	}

	public String buscaPorAno(String ano) {
		String str = "";

        for (Ebook alEbook1 : alEbook) {
            if (alEbook1.getAno().equalsIgnoreCase(ano)) {
                str = str.concat("\n Nome do Arquivo:" + alEbook1.getNomeDoArquivo() + "\n\t Autor:" + alEbook1.getAutor() + "\n\t Editora: " + alEbook1.getEditora() + "\n\t Local: " + alEbook1.getLocal() + "\n\t ano: " + alEbook1.getAno() + "\n\t Descri��o: " + alEbook1.getDescricao() + "\n\t T�tulo: " + alEbook1.getTitulo() + "\n\t N�mero de p�ginas: " + alEbook1.getNumpaginas() + "\n\t G�nero: " + alEbook1.getGenero());
            }
        }
		return str;
        }
        
         public String buscaPorAutor(String autor) {
		String str = "";
        for (Ebook alEbook1 : alEbook) {
            if (alEbook1.getAutor().equalsIgnoreCase(autor)) {
                str = str.concat("\tnomeDoArquivo:" + alEbook1.getNomeDoArquivo() + "\n\t Autor:" + alEbook1.getAutor() + "\n\t Editora: " + alEbook1.getEditora() + "\n\t Local: " + alEbook1.getLocal() + "\n\t ano: " + alEbook1.getAno() + "\n\t Descri��o: " + alEbook1.getDescricao() + "\n\t T�tulo: " + alEbook1.getTitulo() + "\n\t N�mero de p�ginas: " + alEbook1.getNumpaginas()) + "\n\t G�nero: " + alEbook1.getGenero();
            }
        }
		return str;
	}
        
        
/*
    Cadastra as M�dias.
    */
    public void cadastraMidia(Midia e) {
        alEbook.add((Ebook) e);
        ObjectOutputStream boo;
        try {
            boo = new ObjectOutputStream (new FileOutputStream (new File("Ebook.bin")));
            boo.writeObject(alEbook);
            boo.close();
        } catch (IOException ex) {
            Logger.getLogger(EbookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*
    //L�
    */
    public void ler() throws IOException, ClassNotFoundException{
        ObjectInputStream boo;
        boo = new ObjectInputStream (new FileInputStream (new File("Ebook.bin")));
        alEbook = (ArrayList <Ebook>)boo.readObject();
        boo.close();
        
        
    }
    
   
/*
    Valida o ano.
    */
    public boolean validaAno(int ano) {
        return ano >= 1940 && ano <= 2015;
    }
    
/*
    Ordena��o Bubble Sort.
    */
    
    public void ordena() {
        boolean troca = true;
        Ebook aux;
        while (troca) {
            troca = false;
            for (int i = 0; i < alEbook.size() - 1; i++) {
                if (alEbook.get(i).getTitulo().compareTo(alEbook.get(i + 1).getTitulo()) > 0) {
                    aux = alEbook.get(i);
                    alEbook.set(i, alEbook.get(i + 1));
                    alEbook.set(i + 1, aux);
                    troca = true;
                }
            }
        }

    }
}


