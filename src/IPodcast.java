import java.awt.Dimension;
import java.io.FileNotFoundException;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * 
 * @author Abner, Juliano e Camila.
 *
 */
public class IPodcast {
	static PodcastDAO pod;

	public IPodcast(PodcastDAO dao) {
		IPodcast.pod = dao;
	}

	/**
	 * metodo que contem o menu de podcasts é chamado na classe que possui o
	 * metodo main
	 * 
	 * @throws Exception
	 */
	public void executar() throws Exception {
		pod = new PodcastDAO();
		boolean repetir = true;
		while (repetir) {
			int opt = Integer.parseInt(JOptionPane
					.showInputDialog("1- Cadastro e/ou edição\n"
							+ "2- Buscar podcasts\n" + "0- Voltar\n"));
			switch (opt) {
			case 1:
				try {
					menuCadastroEOuEdicao();
				} catch (Exception e) {
					e.getStackTrace();
				}
				break;
			case 2:
				menuBusca();
				break;
			case 0:
				repetir = false;
				break;
			}
		}
	}

	/**
	 * opção de busca do menu de podcasts.
	 */
	public void menuBusca() {
		try {
			pod.ler("Podcasts.bin");
		} catch (FileNotFoundException f) {
			f.getStackTrace();
		} catch (Exception e) {
			e.getStackTrace();
		}
		int a = Integer.parseInt(JOptionPane
				.showInputDialog("1-Busca por idioma\n"
						+ "2-Busca por duração\n" + "3-Busca por autores\n"));
		if (a == 1)
			exibeIdioma();
		else if (a == 2)
			exibeDuracao();
		else if (a == 3)
			exibeAutores();
	}

	/**
	 * exibe a busca de podcasts com os mesmos autores.
	 */
	public void exibeAutores() {
		String busca = JOptionPane
				.showInputDialog("Insira o/os autor/autores do podcast: ");
		JOptionPane.showMessageDialog(null, pod.consultaAutores(busca));
	}

	/**
	 * exibe a busca de podcasts com mesma duração.
	 */
	public void exibeDuracao() {
		String busca = JOptionPane
				.showInputDialog("Insira a duração do podcast: ");
		JOptionPane.showMessageDialog(null, pod.consultaDuracao(busca));
	}

	/**
	 * exibe os resultados da busca por podcats com um mesmo idioma.
	 */
	public void exibeIdioma() {
		String busca = JOptionPane
				.showInputDialog("Insira o idioma do podcast: ");
		JOptionPane.showMessageDialog(null, pod.consultaIdioma(busca));
	}

	/**
	 * opção de cadastro e edição do menu de podcasts.
	 * 
	 * @throws Exception
	 */
	public void menuCadastroEOuEdicao() throws Exception {
		boolean repetir = true;

		try {
			pod.ler("Podcasts.bin");
		} catch (FileNotFoundException fnf) {
			fnf.getStackTrace();
		} catch (Exception e) {
			e.getStackTrace();
		}
		while (repetir) {

			int op1 = Integer.parseInt(JOptionPane
					.showInputDialog("1- Cadastrar podcast\n"
							+ "2- Editar arquivo\n"
							+ "3- Excluir conteudo do arquivo \n"
							+ "4- Consultar arquivo\n" + "5- Ordenar\n"
							+ "0- Voltar"));

			switch (op1) {
			case 1:
				String nomeDoArquivo = JOptionPane
						.showInputDialog("Digite o nome do arquivo: ");
				JOptionPane.showMessageDialog(null,
						"Não insira um titulo ja existente !");
				String titulo = JOptionPane.showInputDialog("Digite titulo: ");
				for (int i = 0; i < pod.podcasts.size(); i++) {
					if (titulo
							.equalsIgnoreCase(pod.podcasts.get(i).getTitulo())) {
						JOptionPane.showMessageDialog(null,
								"Titulo ja inserido !");
						titulo = JOptionPane
								.showInputDialog("Insira novo titulo: ");
					}
				}

				String descricao = JOptionPane
						.showInputDialog("Digite a descrição: ");
				String autor = JOptionPane
						.showInputDialog("Digite o nome dos autores: ");
				String idioma = JOptionPane
						.showInputDialog("Digite o idioma do podcast: ");
				String duracao = JOptionPane
						.showInputDialog("Digite o tempo de duração: ");
				pod.cadastraPodcast(idioma, duracao, nomeDoArquivo, titulo,
						descricao, autor);

				break;

			case 2:
				JOptionPane.showMessageDialog(null, "Menu de alterações.");
				String tituloP = JOptionPane
						.showInputDialog("Digite o titulo do podcast: ");
				int q = pod.buscaTitulo(tituloP);
				String nomeDoArquivo1 = JOptionPane
						.showInputDialog("Digite o nome do arquivo: ");
				String titulo1 = JOptionPane.showInputDialog("Digite titulo: ");
				String descricao1 = JOptionPane
						.showInputDialog("Digite a descrição: ");
				String autor1 = JOptionPane
						.showInputDialog("Digite o nome dos autores: ");
				String idioma1 = JOptionPane
						.showInputDialog("Digite o idioma do podcast: ");
				String duracao1 = JOptionPane
						.showInputDialog("Digite o tempo de duração: ");
				for (int i = 0; i < pod.podcasts.size(); i++) {
					if (q == i) {
						pod.podcasts.get(i).setTitulo(titulo1);
						pod.podcasts.get(i).setAutor(autor1);
						pod.podcasts.get(i).setIdioma(idioma1);
						pod.podcasts.get(i).setNomeDoArquivo(nomeDoArquivo1);
						pod.podcasts.get(i).setDescricao(descricao1);
						pod.podcasts.get(i).setDuracao(duracao1);
					}
				}
				pod.edita(q, pod.podcasts.get(q));
				break;
			case 3:
				String tituloPod = JOptionPane
						.showInputDialog("Digite o titulo: ");

				pod.exclui(tituloPod);
				break;
			case 4:
				JTextArea textArea = new JTextArea(pod.exibe());
				JScrollPane scrollPane = new JScrollPane(textArea);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				scrollPane.setPreferredSize(new Dimension(300, 300));
				JOptionPane.showMessageDialog(null, scrollPane, "Podcasts",
						JOptionPane.PLAIN_MESSAGE);
				break;

			case 5:
				pod.ordena();
				break;
			case 0:

				repetir = false;
				break;
			}// fim menu podcast
			
			try {
				pod.gravar("Podcasts.bin");//salva os dados no arquivo.
			} catch (Exception e) {
				e.getStackTrace();
			}
		}
	}
}