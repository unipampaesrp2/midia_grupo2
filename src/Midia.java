import java.io.Serializable;

/**
 * Contém os atributos comuns a cada tipo de mídia e implementa os métodos gets e sets.
 * @author Juliano, Abner, Camila.
 */
public abstract class Midia implements Serializable{

    private String nomeDoArquivo;
    private String titulo;
    private String descricao;
    private String autor;

    public Midia() {
    }

    public Midia(String nomeDoArquivo, String titulo, String descricao, String autor) {
        this.nomeDoArquivo = nomeDoArquivo;
        this.titulo = titulo;
        this.descricao = descricao;
        this.autor = autor;

    }

    public void setNomeDoArquivo(String nomeDoArquivo) {
        this.nomeDoArquivo = nomeDoArquivo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getNomeDoArquivo() {
        return nomeDoArquivo;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getAutor() {
        return autor;
    }
}
