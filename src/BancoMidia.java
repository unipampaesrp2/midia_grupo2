import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Possui a assinatura dos métodos, que as classes tem em comum, e todas deverão
 * implementar.
 *
 * @author Juliano, Abner, Camila
 */
public interface BancoMidia {

    /**
     * Cadastra uma nova midia no banco de mídias.
     *
     * @param m a midia que será cadastrada.
     */
    public void cadastraMidia(Midia m);

    /**
     * Exibe um determinado tipo de mídia.
     *
     * @return
     */
    public String exibe()throws FileNotFoundException, IOException;

    /**
     * Consulta os dados de um determinado tipo de mídia, através de uma String
     * passada por parâmetro.
     *
     * @param nome String para buscar os dados a serem informados.
     * @return
     */
    public String consulta(String nome);

    /**
     * Eclui um objeto de um determinado tipo de mídia.
     *
     * @param op posição do objeto que será removido.
     */
    public void exclui(int op);

    /**
     * Edita determinado objeto, através da posição dele no arrayList.
     *
     * @param posObjeto posicao do objeto no arrayList.
     * @param novoDado novo dado para ser atribuido ao objeto.
     */
    public void edita(int posObjeto, Midia novoDado);

    /**
     * Ordena os objetos do arquivo, que acordo com um metodo de ordenação.
     */
    public void ordena();
}
