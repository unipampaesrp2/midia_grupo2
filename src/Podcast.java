import java.io.Serializable;

/**
 * 
 * @author Abner
 *
 */
@SuppressWarnings("serial")
public class Podcast extends Midia implements Serializable {
	// atributos de classe.
	private String idioma;
	private String duracao;

	/**
	 * Construtor principal.
	 * 
	 * @param idioma
	 * @param duracao
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param autor
	 */
	public Podcast(String idioma, String duracao, String nomeDoArquivo,
			String titulo, String descricao, String autor) {
		super(nomeDoArquivo, titulo, descricao, autor);
		this.idioma = idioma;
		this.duracao = duracao;
	}

	/**
	 * modifica idioma.
	 * 
	 * @param idioma
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * modifica duração
	 * 
	 * @param duracao
	 */
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

	/**
	 * retorna o idioma
	 * 
	 * @return
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * retorna a duração.
	 * 
	 * @return
	 */
	public String getDuracao() {
		return duracao;
	}

}